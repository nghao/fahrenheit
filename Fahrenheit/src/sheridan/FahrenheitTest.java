package sheridan;

/**
 * 
 * @author hao nguyen - 991521091
 *
 */

import static org.junit.Assert.*;

import org.junit.Test;

public class FahrenheitTest {

	@Test
	public void testFromCelsiusRegular() {
		int fah =Fahrenheit.fromCelsius(15);
		assertTrue(fah == 59);
	}
	
	@Test
	public void testFromCelsiusException() {
		int fah =Fahrenheit.fromCelsius('a');
		assertFalse(fah == 'a');
	}
	
	@Test
	public void testFromCelsiusBoundaryIn() {
		int fah =Fahrenheit.fromCelsius(1);
		assertTrue(fah == 34);
	}
	
	@Test
	public void testFromCelsiusBoundaryOut() {
		int arg = 999999999*999999999;
		int fah =Fahrenheit.fromCelsius(arg);
		assertFalse(1800000030 == fah);
	}
}