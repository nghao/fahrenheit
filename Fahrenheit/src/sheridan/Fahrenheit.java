package sheridan;

/**
 * 
 * @author hao nguyen - 991521091
 *
 */

public class Fahrenheit {
 public static int fromCelsius(int celsius) {
	 double fahrenheit = celsius * 1.8 + 32;
	 return (int) Math.round(fahrenheit);
 }
}
